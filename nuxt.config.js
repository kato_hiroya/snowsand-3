const baseHost = 'https://www.snowsand.jp'
const baseDir = '/'
const baseUrl = baseHost + baseDir
const lang = 'ja'
const siteName = 'SNOW SAND ＜スノーサンド＞'
const siteDesc = '冬が来るとやってくる。放牧牛乳を使ったしっとり生チョコレートをカリッとしたラングドシャクッキーでサンドした冬季限定のお菓子です。いつかの冬に雪降る中で感じたドキドキやワクワクを思い出しながら、美味しいコーヒーとお菓子のひとときをお楽しみください。'
const ogpImages = baseUrl + 'assets/img/'

// GTM
const gtmID = 'GTM-WHL89Z3' // 本番
const gtmHeadTag = `(function (w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','${gtmID}');`
const gtmBodyTag = `<iframe src="https://www.googletagmanager.com/ns.html?id=${gtmID}" height="0" width="0" style="display:none;visibility:hidden"></iframe>`

// gtag
const gaID = 'G-MPWTK7H3CM' // 本番
const gaCode = `window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', '${gaID}');`

// gtag 予備のUA 上記で確認OKもらえれば一式削除
const uaID = 'UA-187205026-1';
const uaCode = `window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', '${uaID}');`


export default {

  ssr: true,

  loading: false,

  target: 'static',

  srcDir: 'src/',

  router: {
    base: baseDir,
    extendRoutes (routes, resolve) {
      routes.push({
        name: '404 error',
        path: '*',
        component: resolve('~/pages/404.vue')
      })
    }
  },

  generate: {
    fallback: '404.html',
  },

  modules: [
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
  ],

  robots: {
    UserAgent: '*',
    Disallow: '/404.html',
    Sitemap: 'https://www.snowsand.jp/sitemap.xml',
  },

  sitemap: {
    path : '/sitemap.xml',
    hostname: 'https://www.snowsand.jp/',
    exclude: [
      '/404',
    ],
    routes: [
      {
        url: '/',
        priority: 1,
      },
      {
        url: '/gallery/',
        priority: 0.8,
      },
      {
        url: '/privacy/',
        priority: 0.4,
      },

    ]
  },

  head: {

    htmlAttrs: {
      lang: 'ja',
      prefix: 'og: http://ogp.me/ns#'
    },

    titleTemplate: `%s${siteName}`,

    meta: [
      { charset: 'utf-8' },
      { 'http-equiv': 'x-ua-compatible', content: 'ie=edge' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: siteDesc },
      { hid: 'description', name: 'description', content: siteDesc },
      // OGP関連
      { hid: 'og:site_name', property: 'og:site_name', content: siteName },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:url', property: 'og:url', content: baseUrl },
      { hid: 'og:title', property: 'og:title', content: siteName },
      { hid: 'og:description', property: 'og:description', content: siteDesc },
      { hid: 'og:image', property: 'og:image', content: `${ogpImages}ogp.jpg` },
      { name: 'twitter:card', content: 'summary_large_image' },
    ],

    link: [
      { rel: 'shortcut icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/assets/icons/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/assets/icons/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/assets/icons/favicon-16x16.png' },
      { rel: 'manifest', href: '/assets/icons/site.webmanifest' },
      { rel: 'mask-icon', color: '#0b629c', href: '/assets/icons/safari-pinned-tab.svg' },
      { name: 'msapplication-TileColor', content: '#0b629c' },
      { name: 'msapplication-config', content: '/assets/icons/browserconfig.xml' },
      { name: 'theme-color', content: '#ffffff' },
    ],

    script: [

      { hid: 'gtmHead', innerHTML: gtmHeadTag },

      { hid: 'gaSrc', async: true, src: 'https://www.googletagmanager.com/gtag/js?id=' + gaID },
      { hid: 'gaCode', innerHTML: gaCode },

      // 予備
      { hid: 'uaSrc', async: true, src: 'https://www.googletagmanager.com/gtag/js?id=' + uaID },
      { hid: 'uaCode', innerHTML: uaCode },

    ],

    noscript: [
      { hid: 'gtmBody', innerHTML: gtmBodyTag, pbody: true }
    ],

    __dangerouslyDisableSanitizersByTagID: {
      'gtmHead': ['innerHTML'],
      'gtmBody': ['innerHTML'],
      'gaSrc': ['innerHTML'],
      'gaCode': ['innerHTML'],
      'uaSrc': ['innerHTML'],
      'uaCode': ['innerHTML']
    },

  },

  plugins: [
    '~/plugins/init.js',
  ],

  css: [
    { src: '@/assets/scss/style.scss', lang: 'scss' },
  ],

  babel: {
    presets({ envName }) {
      const envTarget = {
        server: { node: 'current' },
        client: {},
      };
      return [
        require.resolve('@nuxt/babel-preset-app'),
        {
          targets: envTarget[envName],
          useBuiltIns: 'usage',
          corejs: { version: 3 }
        }
      ]
    }
  },


  build: {

    extractCSS: true,

    postcss: {
      plugins : {
        'postcss-custom-properties' : {
          warnings: false
        },
        'cssnano' : {
          preset: 'default'
        }
      }
    },

    publicPath: '/app/',

    terser: {
      terserOptions: {
        compress: {
          drop_console: true
        }
      }
    },

    extend(config, ctx) {

      // if (ctx.isDev && ctx.isClient) {
      //   config.module.rules.push({
      //     enforce: "pre",
      //     test: /\.(js|vue)$/,
      //     loader: "eslint-loader",
      //     exclude: /(node_modules)/
      //   })
      // }

    },
  },

}
