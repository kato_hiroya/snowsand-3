// ページ遷移関連
// barba.js v2
//=============================================================

import { BannerTimer } from './_banner-timer.js';

window.isTransition = false;

barba.init({

  timeout: 5000,

  prevent: ({ el }) => el.classList && (el.classList.contains('scroll-to') || el.classList.contains('js-m')),

  transitions: [{

    leave(data) {
      isTransition = true;
      resetPageStatus();
    },
    afterLeave(data) {},
    beforeEnter(data) {

      currentPage = data.next.namespace;
      data.next.container.querySelector('.scroll-window').classList.add('next-container');
      gsap.set(data.next.container.querySelector('.scroll-window'), { y: __WH__ });

      init();

    },
    enter(data) {

      const done = this.async();

      gsap.to(data.current.container.querySelector('.scroll-window'), {
        duration: 1.3,
        delay: .8,
        opacity: .4,
        y: -__WH__,
        ease: 'Quint.easeOut'
      });
      gsap.to(data.next.container.querySelector('.scroll-window'), {
        duration: 1.2,
        delay: .8,
        y: 0,
        ease: 'Quint.easeOut',
        onComplete() {
          done();
          pageScroll.reset();
          data.next.container.querySelector('.scroll-window').classList.remove('next-container');
          isTransition = false; // last
        }
      });

    }

  }],

  views: [{

    namespace: 'home',
    beforeEnter() {
      $html.classList.add('page-home');
      evPopupStandby();
      let banner = new BannerTimer();
    },
    afterEnter() {
      if (loading.isLoaded) {
        evPopupOpen();
      }
    },
    beforeLeave() {
      evPopupClose();
      if (stickyScroll) {
        stickyScroll.destroy();
      }
      $html.classList.remove('is-banner-hidden');
    },
    afterLeave() {
      $html.classList.remove('page-home');
    },

  }, {

    namespace: 'gallery',
    beforeEnter() {
      $html.classList.add('page-gallery');
    },
    // afterEnter() {},
    beforeLeave() {
      $html.classList.remove('is-profile-open');
    },
    afterLeave() {
      $html.classList.remove('page-gallery');
    },

  }, {

    namespace: 'sub',
    beforeEnter() {
      $html.classList.add('page-sub');
    },
    // afterEnter() {},
    // beforeLeave() {},
    afterLeave() {
      $html.classList.remove('page-sub');
    },

  }]
});

window.resetPageStatus = () => {
  $html.classList.remove('is-navi-open');
  $body.classList.remove('invert');
};



