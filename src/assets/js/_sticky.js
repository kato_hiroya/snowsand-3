// インスタンス : stickyScroll
// <home> 特徴セクション (SCROLL)
//=============================================================

export class Sticky {

  constructor() {
    this.easing = gsap.parseEase('0.365, 0.155, 0.100, 1.000');
    this.createStickyArray();
  }

  destroy() {
    this.sticky = [];
  }

  createStickyArray() {

    const register = (el, i, array) => {
      const $parent = document.querySelector('[data-area=' + el.dataset.scrollTarget + ']');
      array.push({
        el: el,
        x: 0,
        y: 0,
        name: el.dataset.scrollTarget,
        rect: null,
        parent: $parent,
        parentRect: null,
        inTabArea: false,
      });
    };
    this.sticky = [];
    this.$stickyContent = document.querySelectorAll('[data-barba-namespace="' + currentPage + '"] .sticky-content');
    this.$stickyContent.forEach((el, i) => {
      register(el, i, this.sticky);
    });
    this.onResize();

  }

  onResize() {

    this.sticky.forEach((v, i) => {
      const rect = v.el.getBoundingClientRect();
      const parentRect = (v.parent) ? v.parent.getBoundingClientRect() : null;
      v.rect = {
        bottom: rect.bottom + pageScroll.blankRv + pageScroll.y,
        height: rect.height,
        left: rect.left,
        right: rect.right,
        top: rect.top + pageScroll.blankRv + pageScroll.y,
        width: rect.width,
        x: rect.x,
        y: rect.y + pageScroll.blankRv + pageScroll.y
      };
      v.parentRect = {
        bottom: parentRect.bottom + pageScroll.blankRv + pageScroll.y,
        height: parentRect.height,
        left: parentRect.left,
        right: parentRect.right,
        top: parentRect.top + pageScroll.blankRv + pageScroll.y,
        width: parentRect.width,
        x: parentRect.x,
        y: parentRect.y + pageScroll.blankRv + pageScroll.y
      };
    });

  }

  updateDesktop() {
    this.onUpdate();
  }
  onUpdate() {

    if (!this.sticky.length) return;

    this.sticky.forEach((v, i) => {

      if (v.parentRect) {

        if (!PC && __WW__ <= tabletStyle) return;

        const min = v.parentRect.bottom - pageScroll.y;
        const max = pageScroll.y + __WH__;
        const ratio = (pageScroll.y - v.parentRect.top) / v.parentRect.height;
        const segment = v.parentRect.height - v.rect.height;

        if (min <= 0) {
          // 完全に通り過ぎた後

          gsap.set(v.el, { visibility: 'hidden' });

        } else if (((v.parentRect.top) < max && min > 0)) {
          // エリアが見えている間

          gsap.set(v.el, { visibility: 'visible' });
          //==
          if (v.name === 'features' && v.parentRect.top + (segment + __WH__ / 3) > max) {
            v.y = max - __WH__ - (v.parentRect.top - pageScroll.blankRv);
            v.el.style.transform = 'translate3d(0, ' + v.y + 'px, 0)';
          }
          //==
          if (v.name === 'package' && v.parentRect.top + (segment + __WH__) > max) {
            v.y = max - __WH__ - (v.parentRect.top - pageScroll.blankRv);
            document.querySelector('.background-package').style.transform = 'translate3d(0, ' + -v.y / 10 + 'px, 0)';
          }
          //==
          if (ratio >= .8) {
            this.transform(v, ratio, 'after');
          } else {
            this.transform(v, ratio, 'in');
          }

        } else {
          // エリアより上

          const callback = () => {
            gsap.set(v.el, { visibility: 'hidden' });
          };
          this.transform(v, ratio, 'before', callback);

        }

      }
    });

  }

  transform(v, ratio, state, callback) {

    if (v.name === 'features') {

      if (state === 'before') {

        if (v.inTabArea) {
          v.inTabArea = false;
        }

      } else if (state === 'in') {

        if (!v.inTabArea) {
          v.inTabArea = true;
          //==
          gsap.set('.features-tab', { opacity: 1 });
          const $option = document.querySelector('.features-option-body[data-tab-option="1"]');
          featureTab.update($option, $option.dataset.tabOption, 100);

        } else {

          let $option = null;
          if (ratio <= 0) {
            $option = document.querySelector('.features-option-body[data-tab-option="1"]');

          } else if (ratio <= 0.26) {
            $option = document.querySelector('.features-option-body[data-tab-option="2"]');

          } else {
            $option = document.querySelector('.features-option-body[data-tab-option="3"]');

          }

          featureTab.update($option, $option.dataset.tabOption);

        }

      } else if (state === 'after') {

      }

    } else if (v.name === 'package') {

      if (state === 'before') {

        if (v.inTabArea) {
          v.inTabArea = false;
          //==
          gsap.to('.background-package', {
            duration: .4,
            opacity: 0,
            ease: this.easing,
          });
          gsap.to('.package-img', {
            duration: .4,
            opacity: 0,
            y: -100,
          });
          gsap.to('.package-text', {
            duration: .4,
            opacity: 0,
            onComplete: callback
          });
        }

      } else if (state === 'in') {

        if (ratio > -0.05) {
          gsap.to('.background-package', {
            duration: .4,
            opacity: 0,
            ease: this.easing,
          });
        } else {
          gsap.to('.background-package', {
            duration: .4,
            opacity: 1,
            ease: this.easing,

          });
        }

        if (!v.inTabArea) {
          v.inTabArea = true;
          //==
          gsap.fromTo('.package-img', {
            opacity: 0,
            y: 100,
          }, {
            duration: .5,
            opacity: 1,
            y: 0,
            ease: this.easing,
          });

          gsap.set('.package-text', { opacity: 1 });
          gsap.fromTo('.package-text .s-section-title, .package-text .block-title, .package-text .block-body, .package-text .block-buttonarea', {
            opacity: 0,
            y: 100,
          }, {
            stagger: .1,
            duration: .5,
            delay: .0,
            opacity: 1,
            y: 0,
            ease: this.easing,
          });

        }

      }

    } else if (v.name === 'maker') {

      if (state === 'before') {

        if (v.inTabArea) {
          v.inTabArea = false;
          //==
          gsap.to('.maker-img', {
            duration: .4,
            opacity: 0,
            y: -100,
          });
          gsap.to('.maker-text', {
            duration: .4,
            opacity: 0,
            onComplete: callback
          });
        }

      } else if (state === 'in') {

        if (!v.inTabArea) {
          v.inTabArea = true;
          //==
          gsap.fromTo('.maker-img', {
            opacity: 0,
            y: 100,
          }, {
            duration: .5,
            opacity: 1,
            y: 0,
            ease: this.easing,

          });
          gsap.set('.maker-text', { opacity: 1 });
          gsap.fromTo('.maker-text .s-section-title, .maker-text .block-title, .maker-text .block-body', {
            opacity: 0,
            y: 100,
          }, {
            stagger: .1,
            duration: .5,
            delay: .0,
            opacity: 1,
            y: 0,
            ease: this.easing,
          });
        }

      } else if (state === 'after') {

      }
      //

    }
  }

}
